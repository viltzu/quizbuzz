import React, { Component, PropTypes } from 'react';
import { StyleSheet, View, ScrollView, LayoutAnimation, Platform, UIManager, Modal } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Text, Icon, Grid, Col, Row, Card, CardItem } from 'native-base';
import QuestionButton from './Elements/QuestionButton';

let questionOptions = [
    { 'text': 'Väri' },
    { 'text': 'Maa' },
    { 'text': 'Alkuaine' },
    { 'text': 'Koirarotu' },
    { 'text': 'Koirarotu' },
    { 'text': 'Koirarotu' },
    { 'text': 'Koirarotu' }
];

export default class MainScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
    	time: 30, 
    	count: 0, 
    	questions: [
    		{'multiplier': 0, 'text': 'Not set'},
    		{'multiplier': 0, 'text': 'Not set'},
    		{'multiplier': 0, 'text': 'Not set'}
    	],
    	choosingQuestion: -1
    };

    this.checkAnswer = this.checkAnswer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);

    this.growMultiplier = this.growMultiplier.bind(this);
    //this.RenderQuestions = this.RenderQuestions.bind(this);
    //this.addQuestionButton = this.addQuestionButton.bind(this);
    this.RenderQuestionOptions = this.RenderQuestionOptions.bind(this);
    this.setQuestion = this.setQuestion.bind(this);
    this.openQuestionDrawer = this.openQuestionDrawer.bind(this);

		if (Platform.OS === 'android') {
			UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
		}    
  }

  growMultiplier (_id) {
  	_questions = this.state.questions;
  	_questions[_id].multiplier = _questions[_id].multiplier + 1;
  	if (_questions[_id].multiplier >= 12) {
  		_questions[_id].multiplier = 0;
  		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  	}
  	this.setState({questions: _questions});
  }

  resetTimer() {
  	this.setState({time: 30});
  }

  checkAnswer(_id) {
  	this.openQuestionDrawer(_id);

  	_count = this.state.count + 1;
  	this.setState({count: _count});

		// Start countdown if first press
		if (_count == 1) {
			interval = setInterval(() => {
				_time = this.state.time-1;
				
				if (this.state.count > 0) {
					this.setState({time: _time});
  				if (_time == 0) {
  					clearInterval(interval);
  					this.setState({count: 0});
  					this.resetTimer();
  					this.props.toggleEndScene();
  					return false;
      		}
      	}
  		}, 1000);
		}

		_multiplier = this.state.questions[_id].multiplier;
		console.log(_multiplier);

		if (_multiplier == 0) return true;

  	if (_multiplier == undefined) {
  		for (var i = 0; i < this.state.questions.length; i++) {
  			if (this.state.questions[i].multiplier != 0 && _count % this.state.quesstions[i].multiplier != 0) {
  				this.resetTimer();
  				return true;
  			}
  		}
  	} else if (_count % _multiplier == 0) {
  		this.resetTimer();
  		return true;
  	}

	  clearInterval(interval);
	  this.resetTimer();
  	this.props.toggleEndScene();
  	return false;
  }

  /* // For future? Dynamic question amount?
	RenderQuestions() {
		var that = this;
		var rows = [];

		this.state.multipliers.map(function(_multiplier, i) {
			//if (i+1 == that.state.multipliers.length) {
				rows.push(
					<QuestionButton 
					key={i} id={i} 
					onPress={that.checkAnswer}  
					growMultiplier={that.growMultiplier} 
					mButtonText={_multiplier.toString() + 'x'} />
				);
		});
		return rows;
  } 

  addQuestionButton() {
  	_questions = this.state.questions;
  	_questions.push({'multiplier': 0, 'text': 'Not set'});
  	this.setState({multipliers: _multipliers});
  	//LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  }*/

  /*
  							<Card dataArray={questionOptions}
								renderRow={(question) =>
									<CardItem>
										<Text>{question.text}</Text>
									</CardItem>
								}>
							</Card>
							*/
	RenderQuestionOptions() {
		var that = this;
		var rows = [];

		questionOptions.map(function(_question, i) {
			//if (i+1 == that.state.multipliers.length) {
				rows.push(
					<Button block warning key={i} textStyle={{fontSize: 22, marginLeft: 28}} style={[{flex: 1, maxWidth: NaN}, styles.textButton]} onPress={() => that.setQuestion(that.state.choosingQuestion, _question.text)}>
						{_question.text}
					</Button>
				);
		});
		return rows;
  }

  openQuestionDrawer(_id) {
  	_questions = this.state.questions;
  	_questions[_id].multiplier = 0;
  	this.setState({questions: _questions});
  	this.setState({choosingQuestion: _id});
  	LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  setQuestion(_id, _text) {
  	_questions = this.state.questions;
  	_questions[_id].text = _text;
  	_questions[_id].multiplier = 1;
  	this.setState({questions: _questions});
  	this.setState({choosingQuestion: -1});
  	LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }				

	render() {
		return (
			<Container style={styles.container}> 
				<Content style={styles.content} contentContainerStyle={{flex: 1}}>
					
						<QuestionButton
						id={0} 
						onPress={this.checkAnswer}  
						growMultiplier={this.growMultiplier} 
						multiplier={this.state.questions[0].multiplier} 
						qButtonText={this.state.questions[0].text}/>

						<ScrollView style={{backgroundColor: '#888', position: 'absolute', left: 0, right: 0, height: this.state.choosingQuestion == 0 ? 228 : 0, zIndex: 0, top: 76}}>
							{this.RenderQuestionOptions()}
						</ScrollView>
			
						<QuestionButton style={{top: this.state.choosingQuestion == 0 ? 228 : 0}}
						id={1}
						onPress={this.checkAnswer}
						growMultiplier={this.growMultiplier} 
						multiplier={this.state.questions[1].multiplier} 
						qButtonText={this.state.questions[1].text}/>

						<ScrollView style={{backgroundColor: '#888', position: 'absolute', left: 0, right: 0, 
						height: this.state.choosingQuestion == 1 ? 228 : 0, zIndex: 0, top: 76*2 + (this.state.choosingQuestion == 0 ? 228 : 0)}}>
							{this.RenderQuestionOptions()}
						</ScrollView>

						<QuestionButton style={{top: this.state.choosingQuestion == 0 || this.state.choosingQuestion == 1 ? 228 : 0}}
						id={2} 
						onPress={this.checkAnswer}  
						growMultiplier={this.growMultiplier} 
						multiplier={this.state.questions[2].multiplier} 
						qButtonText={this.state.questions[2].text}/>

						<ScrollView style={{backgroundColor: '#888', position: 'absolute', left: 0, right: 0, 
						height: this.state.choosingQuestion == 2 ? 228 : 0, zIndex: 0, top: 76*3 + (this.state.choosingQuestion == 0 || this.state.choosingQuestion == 1 ? 228 : 0)}}>
							{this.RenderQuestionOptions()}
						</ScrollView>		

					<View style={{flex: 1, bottom: this.state.choosingQuestion != -1 ? -228 : 0, backgroundColor: '#eee'}}>
						<View style={{bottom: 0, flex: 1}}>
							<View style={{flex: 1, flexDirection: 'row', margin: 20}}>
								<Button large info textStyle={{fontSize: 24}} style={{flex: 1, borderRadius: 32}}
									onPress={() => this.checkAnswer(1)}>
									{this.state.time.toString()}s
								</Button>
							</View>		

							<Button block large info textStyle={{fontSize: 24}} onPress={() => this.checkAnswer(-1)}>
								{this.state.count.toString()}
							</Button>  
					
						</View>			

					</View>	
				</Content>

			</Container>
		);
	}
}

const styles = StyleSheet.create({
	container: { },

	content: { backgroundColor: '#FFF' },
	footer: {},

	textButton: {flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center', maxHeight: 64, borderBottomRightRadius: 24, margin: 4, marginLeft: 0}
});

MainScene.propTypes = {
	/*title: PropTypes.string.isRequired,
	onForward: PropTypes.func.isRequired,
	onBack: PropTypes.func.isRequired,*/
	toggleEndScene: PropTypes.func.isRequired
}

MainScene.defaultProps = {
	title: 'MainScene'
}