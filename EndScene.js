import React, { Component, PropTypes } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Icon } from 'native-base';

export default class EndScene extends Component {
	render() {
		return (
	      <Container> 
	          <Header backgroundColor='#999999'>
	              <Title>Quiz Buzz, alpha 0.1, EndScene</Title>

	              <Button transparent>
	                <Icon name='ios-menu' />
	              </Button>
	          </Header>

	          <Content>
	                    
	          </Content>

	          <Footer>
	              <FooterTab>
	                  <Button transparent>
	                      <Icon name='ios-call' />
	                  </Button>  
	              </FooterTab>
	          </Footer>
	      </Container>
		);
	}
}

EndScene.propTypes = {
    /*title: PropTypes.string.isRequired,
    onForward: PropTypes.func.isRequired,
    onBack: PropTypes.func.isRequired,*/
}

EndScene.defaultProps = {
	title: 'EndScene'
}