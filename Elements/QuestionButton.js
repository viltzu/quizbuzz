import React, { Component, PropTypes } from 'react';
import { StyleSheet, View, LayoutAnimation, Platform, UIManager } from 'react-native';
import { Button } from 'native-base';

export default class QuestionButton extends Component {
	constructor(props) {
		super(props);

		this.state = {bFlex: 0};

		//this.growMultiplier = this.growMultiplier.bind(this);
		this.showQuestion= this.showQuestion.bind(this);

		if (Platform.OS === 'android') {
			UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
		}		

		//this.showQuestion();
	}

	componentDidMount () {

		//animation.start();
		//this.showQuestion();
	}

	showQuestion() {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
		this.setState({bFlex: 4});
		this.props.growMultiplier(this.props.id);
	}		

	/*growMultiplier() {
		_multiplier = this.state.multiplier;
		if (_multiplier == 11) {
			_multiplier = 1;
			//this.setMultiplier(index, 1);
		} else {
			_multiplier = _multiplier + 1;
			//this.setMultiplier(index, this.getMultiplier(index) + 1);
		}
		this.state.multiplier = _multiplier;
		this.props.updateMultiplier(this.props.id, _multiplier);
	}*/

	render() {
		if (this.props.multiplier > 0) {
			return (
				<View style={[this.props.style, styles.wrapper]}>
					<Button rounded danger textStyle={{fontSize: 26}} style={styles.multButton} onPress={() => this.props.growMultiplier(this.props.id)}>
						{this.props.multiplier.toString()}x
					</Button>
					<Button warning textStyle={{fontSize: 26, marginLeft: 28}} style={[{flex: 4, maxWidth: NaN}, styles.textButton]} onPress={() => this.props.onPress(this.props.id)}>
						{this.props.qButtonText}
					</Button>
					<View style={{flex: 0}}>
						
					</View>				
				</View>
			);
		} else {
			return (
				<View style={[this.props.style, styles.wrapper]}>
					<Button rounded danger textStyle={{fontSize: 26}} style={styles.multButton} onPress={() => this.showQuestion()}>
						+
					</Button>
					<Button warning textStyle={{fontSize: 26, marginLeft: 28}} style={[{flex: 0, maxWidth: 0}, styles.textButton]}>
						{this.props.qButtonText}
					</Button>
					<View style={{flex: 4}}>
						
					</View>				
				</View>
			);			
		}
	}
}

const styles = StyleSheet.create({
	wrapper: {flex: 1, flexDirection: 'row', maxHeight: 76, margin: 2},

	multButton: {flex: 1, zIndex: 2, height: 72, maxWidth: 72},
	textButton: {flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center', height: 56, borderBottomRightRadius: 32, marginTop: 8, marginLeft: -28}
});

QuestionButton.propTypes = {
	id: PropTypes.number.isRequired,
	onPress: PropTypes.func.isRequired,
	multiplier: PropTypes.number.isRequired,
	qButtonText: PropTypes.string.isRequired,
	growMultiplier: PropTypes.func.isRequired,
	visible: PropTypes.bool.isRequired
}

QuestionButton.defaultProps = {
	multiplier: 0,
	qButtonText: 'Not set',
	visible: true
}