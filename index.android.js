/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Modal
} from 'react-native';


import MainScene from './MainScene';
import EndScene from './EndScene';

export default class quizbuzz extends Component {
  constructor(props) {
    super(props);
    this.state = {endSceneVisible: false};

    // Toggle the state every second
    /*setInterval(() => {
      this.setState({endSceneVisible: !this.state.endSceneVisible});
    }, 4000);*/

    this.toggleEndScene = this.toggleEndScene.bind(this);
  }  

  toggleEndScene() {
    this.setState({endSceneVisible: !this.state.endSceneVisible});
  }

  render() {
    if (this.state.endSceneVisible) {
      return (
        <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.state.endSceneVisible}
        onRequestClose={() => {this.setState({endSceneVisible: false})}}>

          <EndScene />

        </Modal>
      );
    } else {
      return (
        <MainScene toggleEndScene={this.toggleEndScene} />  
      );
    }
  }
}



const styles = StyleSheet.create({
});

AppRegistry.registerComponent('quizbuzz', () => quizbuzz);
